<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'black');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '=bjv5P*@caHN~.Z{J4tV-N`IA6`,K?|+P6Z2l@:5LI3k|1 #EyFr(n{Q}wtYVhC ');
define('SECURE_AUTH_KEY',  '2[c>ndcv9+N5fYKX{/G~IgE|<QZ$0.qC8gPC~LV_-sK+REiR8R=Ik0&J&S{*#$8W');
define('LOGGED_IN_KEY',    ' 9UM- 4-jso6-=0N4_Dy*Ew1ad *xbKEs)|L&R7}b.z9!cEsM :}=Crxpp*(HmK|');
define('NONCE_KEY',        'eF.b5<?RcF @w=TCKf1px%kMNk$G1<q`Km~p6,0{b*Nla^hB6VEJ[Zha%PO6:W`o');
define('AUTH_SALT',        '6_EsRWsOi/72U)nxrcAIGRhv>9[+2Ls?4mwahG3. y2=`cvyiGxeIQT{|)]J%bMO');
define('SECURE_AUTH_SALT', '.+h:=@7x;nKYb!@%n-&=]Yn-5B]4~mcnC^.;+0a?wy@$&z]y!P`%/O/+3& )W=<E');
define('LOGGED_IN_SALT',   'jqcs^CK|wE=8,I9Sxh]X&IHLy;OiV;d{;$OC|459n]&G*-/1?M=0c rlS5m5YL8d');
define('NONCE_SALT',       'i)qGSd$Br@9[8&~!Z<b-}iz@-FLhjO0:$J|{8)]&_Y,wV/^2=>m0%G1t2[`H{Ni4');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
  define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
