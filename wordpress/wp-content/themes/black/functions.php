<?php
require_once("meta-box-class/my-meta-box-class.php");
require_once("theme_option.php");
require_once("ex_widget/intro.php");
add_action('init', 'black_menus');
function black_menus() {
  register_nav_menus(
    array(
      'primary-menu' => __('Menu'),
    )
  );
}
function menu_common($id='',$class=''){
  $defaults = array(
    'theme_location'  => 'primary-menu',
    'menu'            => '',
    'container'       => 'nav',
    'container_class' => $class,
    'container_id'    => $id,
    'menu_class'      => 'menu nav-menu',
    'menu_id'         => '',
    'echo'            => true,
    'fallback_cb'     => 'wp_page_menu',
    'before'          => '',
    'after'           => '',
    'link_before'     => '',
    'link_after'      => '',
    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
    'depth'           => 0,
    'walker'          => ''
  );
  return $defaults;
}
function theme($option){
  $options = get_option('sample_theme_options');
  if(array_key_exists($option,$options)){
    return $options[$option];
  } else {
    return '';
  }

}
function widget(){
  register_sidebar(array(
    'name' => __('First View', 'intro'),
    'id' => 'intro',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '',
    'after_title' => '',
  ));
}
add_action('init','widget');
// add script for admin
add_action('admin_enqueue_scripts', 'wdscript');
function wdscript() {
  wp_enqueue_media();
  wp_enqueue_script('hrw', get_template_directory_uri() . '/ex_widget/upload.js', null, null, true);
}
// end
add_action('widgets_init','widget_intro');
function widget_intro(){
  register_widget('Widget_Intro');
}
function about_us(){
  $arr = array(
    'public' => true,
    'label' => 'About Us',
    'supports' => array(
      'title'
    )
  );
  register_post_type('detail_about', $arr);
}
add_action('init', 'about_us');
$config_about = array(
  'id' => 'about_cover',
  'title' => 'Photos',
  'pages' => array('detail_about'),
  'context' => 'normal',
  'priority' => 'high',
  'fields' => array(),
  'local_images' => true,
  'use_with_theme' => true
);
$about_meta = new AT_Meta_Box($config_about);
$about_meta->addImage('image_field', array('name' => 'Photo'));
$about_meta->addTextarea('intro', array('name' => 'Intro'));
$about_meta->Finish();

function business(){
  $arr = array(
    'public' => true,
    'label' => 'Business',
    'supports' => array(
      'title'
    )
  );
  register_post_type('detail_business', $arr);
}
add_action('init', 'business');
$config_business = array(
  'id' => 'business_cover',
  'title' => 'Photos',
  'pages' => array('detail_business'),
  'context' => 'normal',
  'priority' => 'high',
  'fields' => array(),
  'local_images' => true,
  'use_with_theme' => true
);
$business_meta = new AT_Meta_Box($config_business);
$business_meta->addImage('image_field', array('name' => 'Photo'));
$business_meta->addTextarea('intro', array('name' => 'Intro'));
$business_meta->Finish();
$config_page_template = array(
  'id' => 'page_cover',
  'title' => 'Photos',
  'pages' => array('page'),
  'context' => 'normal',
  'priority' => 'high',
  'fields' => array(),
  'local_images' => true,
  'use_with_theme' => true
);
$page_meta = new AT_Meta_Box($config_page_template);
$page_meta->addImage('image_field', array('name' => 'Photo'));
$page_meta->Finish();
function get_list_post_type($post_type) {
  $posts = new WP_Query(
    array(
      'post_type' => $post_type,
      'post_per_page' => -1,
      'orderby' => 'post_date',
      'order'  => 'asc'
    )
  );
  return $posts;
}
add_filter( 'list_post_type', 'get_list_post_type');
?>