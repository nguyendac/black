<?php get_header();?>
<div id="wrapper" class="wrapper">
  <header id="header" class="header top fixed">
    <?php get_template_part('template/template','header')?>
  </header><!-- end header -->
  <main>
    <?php if ( is_active_sidebar('intro')) : ?>
      <?php dynamic_sidebar('intro'); ?>
    <?php endif; ?>
    <!--/.st_banner-->
    <?php get_template_part('template/section/section','about')?>
    <!--/.st_about-->
    <?php //get_template_part('template/section/section','company')?>
    <!--/.st_company_profile-->
    <?php get_template_part('template/section/section','business')?>
    <!--/.st_business_ctn-->
  </main><!-- end main -->
  <footer id="footer" class="footer">
    <?php get_template_part('template/template','footer')?>
  </footer><!-- end footer -->
</div><!-- end wrapper -->
<script src="<?php bloginfo('template_url')?>/asset/js/libs.js?v=aaf7d84b11fd9617f68dc40a69184eb4"></script>
<script src="<?php bloginfo('template_url')?>/asset/js/reponsive_watcher.js?v=4dc0f121e0bb1b76044771594d1bee0d"></script>
<script src="<?php bloginfo('template_url')?>/asset/js/script.js?v=d8808c4566dfddcb4a6f81a79954ef33"></script>
</body>
</html>