<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 */
get_header();
?>
<div id="wrapper" class="wrapper">
  <header id="header" class="header top fixed">
    <?php get_template_part('template/template','header')?>
  </header><!-- end header -->
  <main>
    <section class="st_page_profile">
      <div class="row">
        <div class="title">
          <h2 class="ttl_section">404</h2>
        </div>
        <div class="main_company_profile">
          <p>Oops! That page can&rsquo;t be found.</p>
        </div>
      </div>
    </section><!-- end news -->
  </main><!-- end main -->
  <footer id="footer" class="footer">
    <?php get_template_part('template/template','footer')?>
  </footer><!-- end footer -->
</div><!-- end wrapper -->
<script src="<?php bloginfo('template_url')?>/asset/js/libs.js?v=aaf7d84b11fd9617f68dc40a69184eb4"></script>
<script src="<?php bloginfo('template_url')?>/asset/js/reponsive_watcher.js?v=4dc0f121e0bb1b76044771594d1bee0d"></script>
<script src="<?php bloginfo('template_url')?>/asset/js/script.js?v=d8808c4566dfddcb4a6f81a79954ef33"></script>
</body>
</html>

