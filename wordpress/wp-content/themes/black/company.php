<?php
/*
Template Name: Company Page
*/
get_header();
?>
<?php if (have_posts()) : while (have_posts()) : the_post();?>
<div id="wrapper" class="wrapper">
  <header id="header" class="header">
    <?php get_template_part('template/template','header')?>
  </header><!-- end header -->
  <main>
    <?php 
    $img = '';
    if(get_post_meta(get_the_ID(),'image_field',true)) {
      $img = get_post_meta(get_the_ID(),'image_field',true)['url'];
    }
    ?>
    <section class="st_page_profile" style="background-image:url(<?php echo $img?>)">
      <h2 class="ttl_section"><span class="row"><?php the_title();?></span></h2>
      <div class="main_company_profile">
        <div class="overplay"></div>
        <div class="row">
          <span><?php _e('Company','black')?></span>
          <p><?php echo nl2br(theme('company_name'))?></p>
          <span><?php _e('Managing Director','black')?></span>
          <p><?php echo nl2br(theme('manage'))?></p>
          <span><?php _e('Address','black')?></span>
          <p><?php echo nl2br(theme('add'))?></p>
          <span><?php _e('Business','black')?></span>
          <p><?php echo nl2br(theme('business'))?></p>
        </div>
      </div>
      <!--/.main_company_profile-->
    </section>
  </main><!-- end main -->
  <footer id="footer" class="footer">
    <?php get_template_part('template/template','footer')?>
  </footer><!-- end footer -->
</div><!-- end wrapper -->
<script src="<?php bloginfo('template_url')?>/asset/js/libs.js?v=aaf7d84b11fd9617f68dc40a69184eb4"></script>
<script src="<?php bloginfo('template_url')?>/asset/js/reponsive_watcher.js?v=4dc0f121e0bb1b76044771594d1bee0d"></script>
<script src="<?php bloginfo('template_url')?>/asset/js/script.js?v=d8808c4566dfddcb4a6f81a79954ef33"></script>
</body>
</html>
<?php endwhile; endif; ?>