<?php
/*
Template Name: Business Page
*/
get_header();
?>
<?php if (have_posts()) : while (have_posts()) : the_post();?>
<div id="wrapper" class="wrapper">
  <header id="header" class="header">
    <?php get_template_part('template/template','header')?>
  </header><!-- end header -->
  <main>
    <section class="st_page_business">
        <div class="overplay"></div>
        <div class="row">
          <h2 class="ttl_section">BUSINESS</h2>
          <div class="bx_custom_w">
            <ul class="bx_business">
              <?php 
              $list =  apply_filters('list_post_type','detail_business');
              while($list->have_posts()):$list->the_post();
              ?>
              <li>
                <a>
                  <figure>
                    <?php if(get_post_meta(get_the_ID(),'image_field',true)['url']) {?>
                      <img src="<?php echo get_post_meta(get_the_ID(),'image_field',true)['url']?>" alt="<?php the_title()?>">
                    <?php } else {?>
                      <img src="http://via.placeholder.com/295x229" alt="<?php the_title()?>">
                    <?php }?>
                  </figure>
                  <span><?php the_title();?></span>
                </a>
              </li>
              <?php endwhile;wp_reset_query();?>
            </ul>
            <!--/.bx_business-->
          </div>
        </div>
      </section>
  </main><!-- end main -->
  <footer id="footer" class="footer">
    <?php get_template_part('template/template','footer')?>
  </footer><!-- end footer -->
</div><!-- end wrapper -->
<script src="<?php bloginfo('template_url')?>/asset/js/libs.js?v=aaf7d84b11fd9617f68dc40a69184eb4"></script>
<script src="<?php bloginfo('template_url')?>/asset/js/reponsive_watcher.js?v=4dc0f121e0bb1b76044771594d1bee0d"></script>
<script src="<?php bloginfo('template_url')?>/asset/js/script.js?v=d8808c4566dfddcb4a6f81a79954ef33"></script>
</body>
</html>
<?php endwhile; endif; ?>