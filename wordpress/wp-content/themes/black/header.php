<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="format-detection" content="telephone=no">
  <meta name="Description" content="<?php bloginfo('description')?>">
  <meta name="Keywords" content="keywords">
  <title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' |'; } ?> <?php bloginfo('name'); ?></title>
  <meta property="og:title" content="<?php bloginfo('name'); ?>">
  <meta property="og:url" content="">
  <meta property="og:image" content="">
  <meta property="og:type" content="website">
  <link rel="stylesheet" href="<?php bloginfo('template_url')?>/asset/css/normalize.css?v=a31ee7fb2ac96745eaa54394a9b18008">
  <link rel="stylesheet" href="<?php bloginfo('template_url')?>/asset/css/animate.css?v=1aaa8beecce19062646761f002462694">
  <link rel="stylesheet" href="<?php bloginfo('template_url')?>/asset/css/reset.css?v=32497932730dde6538ca3df2ad2f236d">
  <link rel="stylesheet" href="<?php bloginfo('template_url')?>/asset/css/style.css?v=5d8e6dc7b9acd0c5e807988448a0da0c">
</head>