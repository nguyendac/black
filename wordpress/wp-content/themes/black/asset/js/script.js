window.requestAnimFrame = (function () {
    return window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        function (callback) {
            window.setTimeout(callback, 1000 / 60);
        };
})();
window.cancelAnimFrame = (function () {
    return window.cancelAnimationFrame ||
        window.cancelRequestAnimationFrame ||
        window.webkitCancelAnimationFrame ||
        window.webkitCancelRequestAnimationFrame ||
        window.mozCancelAnimationFrame ||
        window.mozCancelRequestAnimationFrame ||
        window.msCancelAnimationFrame ||
        window.msCancelRequestAnimationFrame ||
        window.oCancelAnimationFrame ||
        window.oCancelRequestAnimationFrame ||
        function (id) { window.clearTimeout(id); };
})();
function closest(el,selector) {
// type el -> Object
// type select -> String
var matchesFn;
  // find vendor prefix
  ['matches','webkitMatchesSelector','mozMatchesSelector','msMatchesSelector','oMatchesSelector'].some(function(fn) {
    if (typeof document.body[fn] == 'function') {
      matchesFn = fn;
      return true;
    }
    return false;
  })
  var parent;
  // traverse parents
  while (el) {
    parent = el.parentElement;
    if (parent && parent[matchesFn](selector)) {
      return parent;
    }
    el = parent;
  }
  return null;
}
function getCssProperty(elem, property){
   return window.getComputedStyle(elem,null).getPropertyValue(property);
}
var easingEquations = {
  easeOutSine: function (pos) {
      return Math.sin(pos * (Math.PI / 2));
  },
  easeInOutSine: function (pos) {
      return (-0.5 * (Math.cos(Math.PI * pos) - 1));
  },
  easeInOutQuint: function (pos) {
      if ((pos /= 0.5) < 1) {
          return 0.5 * Math.pow(pos, 5);
      }
      return 0.5 * (Math.pow((pos - 2), 5) + 2);
  }
};
function isPartiallyVisible(el) {
  var elementBoundary = el.getBoundingClientRect();
  var top = elementBoundary.top;
  var bottom = elementBoundary.bottom;
  var height = elementBoundary.height;
  return ((top + height >= 0) && (height + window.innerHeight >= bottom));
}
function isFullyVisible(el) {
  var elementBoundary = el.getBoundingClientRect();
  var top = elementBoundary.top;
  var bottom = elementBoundary.bottom;
  return ((top >= 0) && (bottom <= window.innerHeight));
}
window.addEventListener('DOMContentLoaded',function(){
  new MenuSp();
  new Sticky();
})

var MenuSp = (function(){
  function MenuSp(){
    var m = this;
    this._target = document.getElementById('icon_nav');
    this._mobile = document.getElementById('nav');
    this._ele = this._mobile.querySelectorAll('li');
    this.header = document.getElementById('header');
    this._length = this._ele.length;
    this.speed = 50;
    this._timer;
    if(m._target.classList.contains('open')){
      Array.prototype.forEach.call(m._ele,function(el,k){
        el.classList.add('animated');
        el.classList.add('fadeInLeft');
      })
      document.body.style.overflow = 'hidden';
    } else {
      Array.prototype.forEach.call(m._ele,function(el,k){
        el.classList.add('animated');
        el.classList.add('fadeOutLeft');
      })
      document.body.style.overflow = 'inherit';
    }
    this._target.addEventListener('click',function(){
      if(this.classList.contains('open')){
        this.classList.remove('open');
        m._mobile.classList.remove('open');
        document.body.style.overflow = 'inherit';
        document.body.style.position = 'inherit';
        Array.prototype.forEach.call(m._ele,function(el,k){
          setTimeout(function(){
            el.classList.remove('animated');
            el.classList.remove('fadeInLeft');
            el.classList.add('animated');
            el.classList.add('fadeOutLeft');
          },k*m.speed);
          if(k == m._length-1){
            m._timer = setTimeout(function(){
              m._mobile.style.right = '-100%';
            },(k)*m.speed*2)
          }
        })
      } else {
        clearTimeout(m._timer);
        this.classList.add('open');
        m._mobile.classList.add('open');
        Array.prototype.forEach.call(m._ele,function(el,k){
          setTimeout(function(){
            el.classList.remove('animated');
            el.classList.remove('fadeOutLeft');
            el.classList.add('animated');
            el.classList.add('fadeInLeft');
          },k*m.speed)
        })
        m._mobile.style.right = '0';
        document.body.style.overflow = 'hidden';
        document.body.style.position = 'relative';
      }
    })
    this.reset = function(){
      Array.prototype.forEach.call(m._ele,function(el,k){
        el.classList.remove('animated');
        el.classList.remove('fadeOutLeft');
        el.classList.remove('fadeInLeft');
      })
      document.body.style.overflow = 'inherit';
    }
    this.heightMobile = function(){
      if(window.innerWidth < 769) {
        m._mobile.style.height = (window.innerHeight - m.header.clientHeight)+'px';
        m._mobile.style.top = m.header.clientHeight+'px';
      } else {
        m.reset();
        m._mobile.style.height = 'auto';
        m._mobile.style.top = 'inherit';
      }
    }
    this.heightMobile();
    window.addEventListener('resize',function(){
      m.heightMobile();
    })
  }
  return MenuSp;
})()
var Sticky = (function(){
  function Sticky(){
    var s = this;
    this._target = document.getElementById('header');
    this._mobile = document.getElementById('nav');
    this._for_sp = function(top){
      s._target.style.left = 0;
      s._mobile.style.top = s._target.clientHeight+'px';
      document.body.style.paddingTop = s._target.clientHeight+'px';
      if(top >= 0) {
        s._target.classList.add('fixed');
      } else {
        s._target.classList.remove('fixed');
      }
    }
    this._for_pc = function(top,left){
      if(top >= 0) {
        document.body.style.paddingTop = 0;
        s._target.style.left = -left+'px';
      }
    }
    this.handling = function(){
      var _top  = document.documentElement.scrollTop || document.body.scrollTop;
      var _left  = document.documentElement.scrollLeft || document.body.scrollLeft;
      if(window.innerWidth < 769) {
        s._for_sp(_top);
      }  else {
        if(!s._target.classList.contains('top')) {
          s._target.classList.remove('fixed')
        }
        s._for_pc(_top,_left);
      }
    }
    window.addEventListener('scroll',s.handling,false);
    window.addEventListener('resize',s.handling,false);
    window.addEventListener('load',s.handling,false);
  }
  return Sticky;
})()