<?php
/*
Template Name: About Page
*/
get_header();
?>
<?php if (have_posts()) : while (have_posts()) : the_post();?>
<div id="wrapper" class="wrapper">
  <header id="header" class="header">
    <?php get_template_part('template/template','header')?>
  </header><!-- end header -->
  <main>
    <?php 
    $img = '';
    if(get_post_meta(get_the_ID(),'image_field',true)) {
      $img = get_post_meta(get_the_ID(),'image_field',true)['url'];
    }
    ?>
    <section class="st_page_about" style="background-image:url(<?php echo $img?>)">
        <div class="overplay"></div>
        <div class="row">
          <h2 class="ttl_section"><?php the_title()?></h2>
          <div class="bx_custom_w">
            <ul class="bx_about">
              <?php 
              $list =  apply_filters('list_post_type','detail_about');
              while($list->have_posts()):$list->the_post();
              ?>
              <li>
                <p><?php echo nl2br(get_post_meta(get_the_ID(),'intro',true))?></p>
                <figure>
                  <?php if(get_post_meta(get_the_ID(),'image_field',true)['url']) {?>
                    <img src="<?php echo get_post_meta(get_the_ID(),'image_field',true)['url']?>" alt="<?php the_title()?>">
                  <?php } else {?>
                    <img src="http://via.placeholder.com/348x231" alt="<?php the_title()?>">
                  <?php }?>
                </figure>
              </li>
              <?php endwhile;wp_reset_query();?>
            </ul>
            <!--/.bx_about-->
          </div>
        </div>
      </section>
  </main><!-- end main -->
  <footer id="footer" class="footer">
    <?php get_template_part('template/template','footer')?>
  </footer><!-- end footer -->
</div><!-- end wrapper -->
<script src="<?php bloginfo('template_url')?>/asset/js/libs.js?v=aaf7d84b11fd9617f68dc40a69184eb4"></script>
<script src="<?php bloginfo('template_url')?>/asset/js/reponsive_watcher.js?v=4dc0f121e0bb1b76044771594d1bee0d"></script>
<script src="<?php bloginfo('template_url')?>/asset/js/script.js?v=d8808c4566dfddcb4a6f81a79954ef33"></script>
</body>
</html>
<?php endwhile; endif; ?>