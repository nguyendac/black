<?php
class Widget_Intro extends WP_Widget {
  function __construct(){
    parent::__construct(
      'widget_intro',
      'Custom Intro'
    );
  }
  function form($instance){
    parent::form($instance);
    $default = array(
      'intro1' => '',
      'intro2' => '',
      'link'   => '',
      'image'  => ''
    );
    $instance = wp_parse_args((array) $instance, $default);
    $intro1 = esc_attr($instance['intro1']);
    $intro2 = esc_attr($instance['intro2']);
    $link = esc_attr($instance['link']);
    $image = esc_attr($instance['image']);
    echo '<div class="about">';
    echo '<p><label>Intro 1</label><br/>';
    echo '<textarea name="'.$this->get_field_name('intro1').'" class="specical">'.$intro1.'</textarea></p>';
    // echo '<p><label>Intro 2</label><br/>';
    // echo '<textarea name="'.$this->get_field_name('intro2').'" class="specical">'.$intro2.'</textarea></p>';
    // echo '<p><label>Link</label><br/>';
    // echo '<input type="text" name="'.$this->get_field_name('link').'" value="'.$link.'"></p>';
    /* image */
    echo '<p><label>Background</label><br/>';
    if($image!='') {
      echo '<img class="custom_media_image" src="' .$image. '" style="margin:0;padding:0;max-width:100px;display:block" /><br />';
    }
    echo '<input type="text" class="custom_media_url" name="'.$this->get_field_name('image').'" id="'.$this->get_field_id('image').'" value="'.$image.'">';
    echo '<input type="button" id="custom_media_service_1_button" class="button_daniel" name="'.$this->get_field_name('image').'" value="Upload Image">';
    echo '<input type="button" id="custom_media_service_1_button" class="rm_button_daniel" value="Remove Image">';
    echo '</p>';
    /* image */
    echo '</div>';
  }
  function update($new_instance,$old_instance){
    parent::update($new_instance,$old_instance);
    $instance = $old_instance;
    $instance['intro1'] = strip_tags($new_instance['intro1']);
    $instance['intro2'] = strip_tags($new_instance['intro2']);
    $instance['link'] = strip_tags($new_instance['link']);
    $instance['image'] = strip_tags($new_instance['image']);
    return $instance;
  }
  function widget($args,$instance){
    extract($args);
    $img = '';
    if($instance['image']) {
      $img =  $instance['image'];
    }
    echo '<section class="st_banner" style="background-image: url('.$img.')">';
    echo '<h2>'.nl2br($instance['intro1']).'</h2>';
    // echo '<p>'.nl2br($instance['intro2']).'</p>';
    //echo '<a href="'.$instance['link'].'">'.__("LEARN MORE",'black').'</a>';
    echo '</section>';
  }
}
?>