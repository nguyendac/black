jQuery(function($){
  var _custom_media = true,_orig_send_attachment = wp.media.editor.send.attachment;
  $(document).on('click', '.button_daniel', function(e){
    e.preventDefault();
    var send_attachment_bkp = wp.media.editor.send.attachment;
    var self = $(this);
    wp.media.editor.send.attachment = function(props, attachment){
        if ( _custom_media  ) {
            self.parent().find('.custom_media_url').val(attachment.url);
            self.parent().find('.custom_media_image').attr('src',attachment.url).css('display','block');
            // $('.custom_media_id').val(attachment.id);
        } else {
            return _orig_send_attachment.apply( self, [props, attachment] );
        }
    }
    wp.media.editor.open($(this));
      return false;
  })  
  $(document).on('click','.rm_button_daniel',function(e){
    e.preventDefault();
    $(this).parent().find('.custom_media_url').val('');
    $(this).parent().find('.custom_media_image').attr('src','');
  })
});