var direct = {
  1 : {
    'name': 'Hải Châu',
    'wards': {
      1: 'Hải Châu 1',
      2: 'Hải Châu 2',
      3: 'Bình Hiên',
      4: 'Bình Thuận',
      5: 'Thuận Phước',
      6: 'Thanh Bình',
      7: 'Thạch Thang',
      8: 'Phước Ninh',
      9: 'Hòa Thuận Tây',
      10: 'Hòa Thuận Đông',
      11: 'Nam Dương',
      12: 'Hòa Cường Bắc',
      13: 'Hòa Cường Nam'
    }
  },
  2 : {
    'name': 'Thanh Khê',
    'wards': {
      1: 'Thạch Gián',
      2: 'An Khê',
      3: 'Xuân Hà',
      4: 'Thanh Khê Đông',
      5: 'Tam Thuận',
      6: 'Tam Chính',
      7: 'Hoà Khê',
      8: 'Vĩnh Trung',
      9: 'Thanh Khê Tây',
    }
  },
  3 : {
    'name': 'Cẩm Lệ',
    'wards': {
      1: 'Hoà Thọ Tây',
      2: 'Hoà Xuân',
      3: 'Khuê Trung',
      4: 'Hoà An',
      5: 'Hoà Thọ Đông',
      6: 'Hoà Phát',
      7: 'Hoà Khê',
      8: 'Vĩnh Trung',
      9: 'Thanh Khê Tây',
    }
  },
  4 : {
    'name' : "Ngũ Hành Sơn",
    'wards': {
      1: 'Hoà Quí',
      2: 'Hoà Hải',
      3: 'Khuê Mỹ',
      4: 'Mỹ An',
    }
  },
  5 : {
    'name': 'Sơn Trà',
    'wards': {
      1: 'An Hải Đông',
      2: 'An Hải Tây',
      3: 'An Hải Bắc',
      4: 'Nại Hiên Đông',
      5: 'Mân Thái',
      6: 'Thọ Quang',
      7: 'Phước Mỹ',
    }
  }
}
jQuery(document).ready(function($) {
  var count = $('select[name="county"]');
  var ward = $('select[name="ward"]');
  // count.html('');
  // var start = '<option value="">Quận/Huyện</option>';
  // count.append(start);
  // $.each(direct,function(index,county){
  //   var option = '<option value="'+index+'">'+county.name+'</option>';
  //   count.append(option);
  // })
  $(document).on('change',count,function(){
    ward.html('');
    var c = count.select2('val');
    if(c){
      var ph = direct[c].wards;
      $.each(ph,function(index,value){
        var option = '<option value="'+index+'">'+value+'</option>';
        ward.append(option);
      })
    } else {
      ward.select2("val", "");
    }
  });
  $('.colorpicker').wpColorPicker();
});
