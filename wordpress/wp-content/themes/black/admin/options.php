<?php
$prefix = 'mokey_';
$eto_custom_tabs = array(
  array(
    'label'=> __('Tùy Chỉnh Theme', 'eto'),
    'id'    => $prefix.'d'
  )
);
$eto_custom_meta_fields = array(
  array(
    'id'    => $prefix.'d', // Use data in $eto_custom_tabs
    'type'  => 'tab_start'
  ),
  /* Your fields here */
  /* phone */
  array(
    'label'=> 'Số Điện Thoại',
    'id'    => $prefix.'phone',
    'desc'    => 'số điện thoại',
    'type'  => 'text'
  ),
  /* email */
  array(
    'label'=> 'Email',
    'id'    => $prefix.'email',
    'desc' => 'Email',
    'type'  => 'text'
  ),
  /* address */
  array(
    'label'=> 'Địa chỉ',
    'id'    => $prefix.'address',
    'type'  => 'text',
    'desc'=> 'Địa chỉ'
  ),
  /* logo */
  array(
    'label' => 'Logo',
    'desc'  => 'Thay đổi logo',
    'id'    => $prefix.'logo',
    'type'  => 'image'
  ),
  /* thay đổi màu chủ đạo */
  array(
    'label' => 'Màu chủ đạo',
    'desc'  => 'Thay đổi màu sắc của trang',
    'id'    => $prefix.'color',
    'type'  => 'colorpicker'
  ),
  array(
    'type'  => 'tab_end' // Close Tab
  )
);
?>
