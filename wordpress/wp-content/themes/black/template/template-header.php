<div class="main_header row">
  <div class="bottom">
    <div class="left">
      <h1><a href="<?php bloginfo('url')?>">MOONSHOT INTERNATIONAL DMCC</a></h1>
    </div>
    <div class="right">
      <div class="menu_sp">
        <div class="icon_menu" id="icon_nav">
          <div class="icon_inner"></div>
        </div>
      </div>
      <?php wp_nav_menu(menu_common($id='nav',$class='nav_menu'));?>
    </div>
  </div>
</div>