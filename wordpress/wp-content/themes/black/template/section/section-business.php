<section class="st_business_ctn">
  <div class="row">
    <h2 class="ttl_section"><?php _e('Business','black')?></h2>
    <div class="bx_custom_w">
      <ul class="bx_business">
        <?php 
        $list =  apply_filters('list_post_type','detail_business');
        while($list->have_posts()):$list->the_post();
        ?>
        <li>
          <a>
            <span><?php the_title();?></span>
            <figure><img src="<?php echo get_post_meta(get_the_ID(),'image_field',true)['url']?>" alt="<?php the_title();?>"></figure>
          </a>
        </li>
        <?php endwhile;wp_reset_query();?>
      </ul>
      <!--/.bx_business-->
    </div>
    <!--/.bx_custom_w-->
  </div>
</section>