<section class="st_about">
  <div class="row">
    <h2 class="ttl_section"><?php _e('About Us','black')?></h2>
    <div class="bx_custom_w">
      <ul class="bx_about">
        <?php 
        $list =  apply_filters('list_post_type','detail_about');
        $i = 0;
        while($list->have_posts()):$list->the_post();
        $i+=1;
        ?>
        <li>
          <span><?php echo $i;?></span>
          <p><?php echo nl2br(get_post_meta(get_the_ID(),'intro',true))?></p>
        </li>
      <?php endwhile;wp_reset_query();?>
      </ul>
      <!--/.bx_about-->
      </div>
    <!--/.bx_custom_w-->
  </div>
</section>