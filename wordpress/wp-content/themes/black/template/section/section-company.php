<section class="st_company_profile">
  <div class="overplay"></div>
  <div class="row main_company_profile">
    <h2 class="ttl_section"><?php _e('Company’s outline','black')?></h2>
    <div class="bx_custom_w">
      <span><?php _e('Company','black')?></span>
      <p><?php echo nl2br(theme('company_name'))?></p>
      <span><?php _e('Managing Director','black')?></span>
      <p><?php echo nl2br(theme('manage'))?></p>
      <span><?php _e('Address','black')?></span>
      <p><?php echo nl2br(theme('add'))?></p>
      <span><?php _e('Business','black')?></span>
      <p><?php echo nl2br(theme('business'))?></p>
    </div>
    <!--/.bx_custom_w-->
  </div>
</section>